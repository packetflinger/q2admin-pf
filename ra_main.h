#ifndef RA_MAIN_H
#define RA_MAIN_H

#if defined(WIN32)
#include <winsock2.h>
#else
#include <sys/socket.h>
#endif

#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <openssl/rand.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include <curl/curl.h>
#include "game.h"

#define PROTO_REGISTER		"100"
#define PROTO_NOTAUTH		"101"
#define PROTO_AUTHED		"102"
#define PROTO_PING			"103"
#define PROTO_PONG			"104"
#define PROTO_QUIT 			"quit"
#define PROTO_NEWMAP		106
#define PROTO_USERINFO		107

#define PROTO_JOIN			200
#define PROTO_PART			201
#define PROTO_FRAG			202
#define PROTO_CHAT			203

#define PROTO_MUTE			"300"
#define PROTO_KICK			"301"
#define PROTO_BAN			"302"
#define PROTO_BLACKHOLE		"303"

#define PROTO_CMD			"400"
#define PROTO_CMSG			"401"

#define RA_MAX_MSG_SIZE		1024

#define EDICT_NUM(n) ((edict_t *)((byte *)globals.edicts + globals.edict_size*(n)))
#define NUM_FOR_EDICT(e) ( ((byte *)(e)-(byte *)globals.edicts ) / globals.edict_size)

cvar_t *g_ra_library;
cvar_t *g_ra_addr;
cvar_t *g_ra_port;
cvar_t *g_ra_enabled;
cvar_t *g_ra_uniqid;


char *va(const char *format, ...) __attribute__((format(printf, 1, 2)));

void Ra_CopyEdicts(void);
void Ra_Send(char*);
void Ra_ParseInput(void);
void Ra_Init(void);
void Ra_CheckStatus(void);
void Ra_SendPackets(void);
void Ra_ReadPackets(void);
void Ra_RunFrame(void);
void Ra_Connect(void);
void Ra_Disconnect(void);
void Ra_ClientConnect(edict_t *ent, char *user_info);
void Ra_ClientDisconnect(edict_t *ent);
uint32_t Ra_GetClientNumber(edict_t *ent);
void Ra_LoadRealLibrary(const char *lib, _Bool lazy, game_import_t *import);
void Ra_BroadcastPrint(int32_t level, const char *fmt, ...);
void Ra_Status_f(void);
void Ra_Connect_f(void);
void Ra_Disconnect_f(void);
int Ra_GetClientFromIP(char *ip);
void Ra_NewMap(char *map);
void Ra_ClientCommand(edict_t *ent);
void Ra_UserInfoChange(edict_t *ent, char *userinfo);

#define RA_MSGLENMAX 		1024;
#define RA_MSGDELIM 		0x1f // unprintable
#define RA_RECONNECT_SECS 	15

// the SSL connection
typedef struct {
    int	socket;
    SSL	*sslHandle;
    SSL_CTX	*sslContext;
} connection;

typedef struct {
	_Bool		enabled;		// should we do anything?
	_Bool		ready;			// ok to connect?
	_Bool		connected;		// are we currently connected?
	_Bool		connecting;		// are we trying to connect?
	_Bool		authed;			// are we authenticated?
	_Bool		authing;		// are in the process of authenticating?
	char		*server_ip;		// the server we're connected to
	char		*server_port;		// tcp port
	_Bool		report_only;		// only send data, ignore server control msgs
	long		connected_time;		// how long have we been connected?
	float		last_try;		// (server) time of our last connection attempt
	float		last_msg;		// how long since our last msg from server?
	float		last_sent;		// when was the last time we sent a msg?
	char		*current_msg;		// the last msg sent from the server
	int			current_code;		// the current protocol code (the begining of each msg)
	connection	*conn;			// the ssl network connection
	fd_set		fds;			// the file descriptors for the socket to monitor
	fd_set		fds_send;		// file descriptors for sending
	struct		timeval	timeout;	// the timeout for select
	int			socket;			// the socket handle
	int			connection;		// the socket connection handle
	char		msgbuffer[RA_MAX_MSG_SIZE];	// msgs to send
	size_t		msglen;			// low long is the current msgbuffer?
	_Bool		dll_loaded;		// is the real game module loaded?
	char		*map			// the current map
} ra_state_t;

ra_state_t	ra;

typedef struct {
	char	msg[200];
	//ra_msg_q_t *next;
} ra_msg_q_t;

#endif
