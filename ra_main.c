#include "g_local.h"

cvar_t *g_ra_library;	// the real game module to proxy to
cvar_t *g_ra_enabled;	// should we bother with remote functions
cvar_t *g_ra_addr;	// hostname/ipv[46]
cvar_t *g_ra_port;	// tcp port (default 5555)
cvar_t *g_ra_uniqid;	// sha256 hash from remote server\

ra_state_t	ra;


/*
 * Doesn't actually send, just adds to the send buffer
 *
 */
void Ra_Send(char *txt)
{
	// avoid an overflow, just drop the message.
	if (ra.msglen + strlen(txt) > RA_MAX_MSG_SIZE)
	{
		gi.dprintf("Ra_Send(): Message overflow, dropping\n");
		return;
	}
	
	sprintf(ra.msgbuffer + ra.msglen, "%s%c", txt, RA_MSGDELIM);
	ra.msglen += strlen(txt) + 1;
}


/*
 * Called on any input received from server
 *
 */
void Ra_ParseInput()
{
	// check for authed, if so we're ready
	if (strncmp(PROTO_AUTHED, ra.current_msg, 3) == 0)
	{
		ra.connected = true;
		ra.connecting = false;
		ra.authing = false;
		ra.authed = true;
		gi.dprintf("=RemoteAdmin= Authenticated and Ready\n");
		gi.bprintf(PRINT_HIGH, "RemoteAdmin connection established\n");
		Ra_NewMap(ra.map);
		return;
	}

	// command from server
	if (strncmp(PROTO_CMD, ra.current_msg, 3) == 0)
	{
		char *cmd = ra.current_msg + 4;
		gi.dprintf("Command found: \"%s\"", cmd);
		gi.AddCommandString(cmd);
		return;
	}
	
	// kick player command from server
	if (strncmp(PROTO_KICK, ra.current_msg, 3) == 0)
	{
		char *ip = ra.current_msg + 4;
		int client = Ra_GetClientFromIP(ip);
		gi.dprintf("Kick found: %s - %d", ip, client);
		if (client > -1)
		{
			gi.AddCommandString(va("kick %d", client));
		}
		return;
	}
	
	// check for ping, if found, send pong
	if (strncmp(PROTO_PING, ra.current_msg, 3) == 0)
	{
		Ra_Send(PROTO_PONG);
		return;
	}
	
	// client message (goto)
	if (strncmp(PROTO_CMSG, ra.current_msg, 3) == 0)
	{
		ra.current_msg += 4; // skip the protocol and space
		char *ipport = strtok(ra.current_msg, " ");
		int iplen = strlen(ipport);
		ra.current_msg += iplen + 1; // skip over the address:port
		
		int client = Ra_GetClientFromIP(ipport);
		edict_t *ent = EDICT_NUM(client + 1);
		
		if (ent->client != NULL)
		{
			gi.cprintf(ent, PRINT_HIGH, "%s\n", ra.current_msg);
		}
		return;
	}
	
	ra.current_msg = NULL;
}

int Ra_GetClientFromIP(char *ip)
{
	int i;
	for (i=0; i<gi.cvar("maxclients", "8", 0); i++)
	{
		if (strcmp(proxyinfo[i].ipaddress, ip) == 0)
		{
			return i;
		}
	}
	return -1;
}

/*
 * Called when a player connects. Send userinfo for ban/mute checks and 
 * so we know who is on what server.
 *
 */
void Ra_ClientConnect(edict_t *ent, char *user_info)
{
	if (!ra.enabled)
		return;
		
	int client = Ra_GetClientNumber(ent);
	Ra_Send(
		va("%d %d %s", 
			PROTO_JOIN,
			client,
			user_info
		)
	);
}


/*
 * Called when a player disconnects. 
 *
 */
void Ra_ClientDisconnect(edict_t *ent)
{
	int client = Ra_GetClientNumber(ent);
	Ra_Send(
		va("%d %s", 
			PROTO_PART, 
			proxyinfo[client].ipaddress
		)
	);
}


uint32_t Ra_GetClientNumber(edict_t *ent)
{
	int i;
	for (i=0; i<gi.cvar("maxclients", "8", 0); i++)
	{
		if (&globals.edicts[i] == &ent)
		{
			return i;
		}
	}

	return 0;
}


void Ra_BroadcastPrint(int32_t level, const char *fmt, ...) \
{
	if (ra.enabled && ra.connected)
	{
		Ra_Send("chat msg");
	}
	
	char string[1024];
	va_list args;
	//sv_client_t * cl;
	int32_t i;

	va_start(args, fmt);
	vsprintf(string, fmt, args);
	va_end(args);
	
	printf("=RA= %s\n", string);
}


char *sslRead (connection *c)
{
	const int readSize = 256;	// was 1024
	char *rc = NULL;
	int received, count = 0;
	char buffer[1024];

	if (c)
	{
		while (1)
		{
			if (!rc)
			{
				rc = malloc (readSize * sizeof (char) + 1);
			}
			else
			{
				rc = realloc (rc, (count + 1) * readSize * sizeof (char) + 1);
			}

			received = SSL_read (c->sslHandle, buffer, readSize);
			buffer[received] = '\0';

			// stuff in the buffer
			if (received > 0)
			{
				strcat(rc, buffer);
			}

			// an error occurred
			if (received == 0)
			{
				switch (SSL_get_error(c->sslHandle, received))
				{
					case SSL_ERROR_ZERO_RETURN:
						Ra_Disconnect();
						break;
					case SSL_ERROR_SSL:
						Ra_Disconnect();
						break;
					default:
						gi.dprintf("Error in SSL_read(), but not serious\n");
				}
			}
		
			// we read the entire buffer
			if (received < readSize)
			{
				break;
			}
			count++;
		}
	}
	else
	{
		gi.dprintf("not connected\n");
	}

	return rc;
}


// not using separate thread - remove later
void *Ra_Receive_Thread(void *remote)
{
	char *reply;
	while (1)
	{
		if (ra.connected)
		{
			reply = sslRead(ra.conn);
			ra.current_msg = reply;
			Ra_ParseInput();
		}
	}
	free(reply);
}



void Ra_Init()
{
	if (g_ra_enabled->value == 0)
	{
		gi.dprintf("=RemoteAdmin= Disabled...\n");
		ra.enabled = false;
		return;
	}

	memset(&ra, 0, sizeof(ra));
	
	gi.dprintf("Initializing RemoteAdmin\n");
	ra.enabled 		= true;
	ra.connected 	= false;
	ra.connecting 	= false;
	ra.authed 		= false;
	ra.authing 		= false;
	ra.server_ip 	= g_ra_addr->string;
	ra.server_port 	= g_ra_port->string;
	ra.connected_time = 0;
	ra.last_try 	= 999;	// fire connect immediately on next frame

	ra.timeout.tv_sec	= 0;	// don't wait to return
	ra.timeout.tv_usec	= 0;	// ""

	memset(ra.msgbuffer, 0, RA_MAX_MSG_SIZE);
	ra.msglen = 0;
	ra.ready = true;
}




/*
 * called every frame to check if we should (re)connect
 *
 */
void Ra_CheckStatus(void)
{
	if (!ra.connected && !ra.connecting) 
	{
		if (ra.last_try > RA_RECONNECT_SECS) 
		{
			Ra_Connect();
		}
		ra.last_try += 0.1f;
	}
}

/*
 * Send the buffer if there is anything pending
 *
 */
void Ra_SendPackets()
{
	if (ra.msglen > 0)
	{
		// add a newline
		ra.msgbuffer[ra.msglen] = '\n';
		ra.msglen++;
		
		int ret = send(ra.socket, ra.msgbuffer, ra.msglen, 0);
		if (ret > -1)
		{
			// clear everything
			ra.msglen = 0;
			memset(ra.msgbuffer, 0, RA_MAX_MSG_SIZE);
		}
	}
}
 

/*
 * Run every frame, looks for incoming packets from ra server.
 * Also sends contents of send buffer if socket allows sending
 * at the time.
 *
 */
void Ra_ReadPackets()
{
	if (ra.connected || ra.connecting)
	{	
		int current_error, ret;
		FD_ZERO(&ra.fds);
		FD_ZERO(&ra.fds_send);
		FD_SET(ra.socket, &ra.fds);
		FD_SET(ra.socket, &ra.fds_send);

		// check the socket for any new msgs
		ret = select(ra.socket + 1, &ra.fds, &ra.fds_send, NULL, &ra.timeout);
		if (ret == -1)
		{
			current_error = errno;
			gi.dprintf("=RemoteAdmin= select() error (%d)\n", current_error);
		}

		// send any pending messages if socket is free to send
		if (FD_ISSET(ra.socket, &ra.fds_send) && ra.connected)
		{
			Ra_SendPackets();
		}
		
		// new data in from the wire
		if (FD_ISSET(ra.socket, &ra.fds))
		{
			char *reply;
			char inbuffer[1024];
			memset(&inbuffer, 0, 1024);
			int incount;
			incount = recv(ra.socket, inbuffer, 1024, 0);
			if (incount == 0)
			{
				gi.dprintf("=RemoteAdmin= connection lost...\n");
				ra.connected = false;
				ra.connecting = false;
				ra.authed = false;
				return;
			}

			if (incount == -1)
			{
				gi.dprintf("=RA= read error\n");
				return;
			}

			inbuffer[incount] = '\0';

			ra.current_msg = va("%s", inbuffer);
			gi.dprintf("=RemoteAdmin= input: \"%s\" (incount: %d)\n", ra.current_msg, incount);
			Ra_ParseInput();
		}
	
		// authenticate as soon as the socket allows
		if (FD_ISSET(ra.socket, &ra.fds_send) && ra.connected && !ra.authed && !ra.authing)
		{
			gi.dprintf("=RemoteAdmin= registering with remote host\n");
			Ra_Send(va("%s %s", PROTO_REGISTER, g_ra_uniqid->string));
			ra.authing = true;
		}
	}
}


/*
 * run every frame (1/10 second). Called from G_RunFrame()
 *
 */
void Ra_RunFrame()
{
	if (ra.enabled) 
	{
		Ra_ReadPackets();
		Ra_CheckStatus();
	}
}



int Cvar_Match(char *cvar, char *val)
{
	if (!strncmp(cvar, val, 10))
	{
		return 1;
	}
	return 0;
}



int tcpConnect ()
{
	int error, handle;
	struct hostent *host;
	struct sockaddr_in server;

	host = gethostbyname (ra.server_ip);
	handle = socket (AF_INET, SOCK_STREAM, 0);
	if (handle == -1)
	{
		gi.dprintf("* RA: couldn't create socket\n");
		perror ("Socket");
		handle = 0;
	}
	else
	{
		server.sin_family = AF_INET;
		server.sin_port = htons (atoi(ra.server_port));
		server.sin_addr = *((struct in_addr *) host->h_addr);
		bzero (&(server.sin_zero), 8);

		error = connect(handle, (struct sockaddr *) &server,
					   sizeof(struct sockaddr));
		if (error == -1)
		{
			gi.dprintf("* RA: couldn't connect using created socket\n");
			perror("Connect");
			handle = 0;
		}
	}

	return handle;
}



connection *sslConnect (void)
{
	connection *c;

	c = malloc (sizeof (connection));
	c->sslHandle = NULL;
	c->sslContext = NULL;

	c->socket = tcpConnect ();
	if (c->socket)
	{
		// Register the error strings for libcrypto & libssl
		SSL_load_error_strings ();
		// Register the available ciphers and digests
		SSL_library_init ();

		// New context saying we are a client, and using SSL 2 or 3
		c->sslContext = SSL_CTX_new (SSLv23_client_method ());
		if (c->sslContext == NULL)
			ERR_print_errors_fp (stderr);

		// Create an SSL struct for the connection
		c->sslHandle = SSL_new (c->sslContext);
		if (c->sslHandle == NULL)
			ERR_print_errors_fp (stderr);

		// Connect the SSL struct to our connection
		if (!SSL_set_fd (c->sslHandle, c->socket))
			ERR_print_errors_fp (stderr);

		// Initiate SSL handshake
		if (SSL_connect (c->sslHandle) != 1)
			ERR_print_errors_fp (stderr);
	}
	else
	{
		gi.dprintf("* RA: SSL connection failed\n");
		//perror ("Connect failed");
	}

	return c;
}


// Disconnect & free connection struct
void sslDisconnect(connection *c)
{
	if (c->socket)
		close (c->socket);
		
	if (c->sslHandle)
	{
		SSL_shutdown (c->sslHandle);
		SSL_free (c->sslHandle);
	}
	
	if (c->sslContext)
		SSL_CTX_free (c->sslContext);

	free (c);
}



// Write text to the connection
void sslWrite(connection *c, char *text)
{
	if (c)
	{
		SSL_write(c->sslHandle, text, strlen (text));
	}
}

/*
 * Connect to the Remote Admin server over tcp
 *
 */
void Ra_Connect()
{
	if (!ra.ready)
	{
		return;
	}

	ra.connected = false;
	ra.connecting = true;
	ra.authed = false;
	ra.authing = false;
	ra.connection = 1;		// will be 0 for success, -1 for failure
	ra.last_try = 0.0f;
	
	gi.dprintf(va("=RemoteAdmin= Connecting to %s:%s\n", ra.server_ip, ra.server_port));
	ra.conn = NULL;

	struct addrinfo hints, *res;
	int error, current_error;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	// lookup the name/addr and get all sorts of useful into
	error = getaddrinfo(g_ra_addr->string, g_ra_port->string, &hints, &res);
	if (error) {
		ra.connecting = false;
		ra.connected = false;
		ra.ready = false;
		gi.dprintf("=RemoteAdmin= problems resolving RA server address....giving up.\n");
		return;
	}
	
	// create a non-blocking socket
	ra.socket = socket(
		res->ai_family, 
		res->ai_socktype | SOCK_NONBLOCK,
		res->ai_protocol
	);
	
	if (ra.socket < 0)
	{
		ra.connecting = false;
		ra.connected = false;
		ra.enabled = false;
		ra.ready = false;
		gi.dprintf("=RemoteAdmin= problems creating socket, aborting\n");
		return;
	}

	int nodelayopt = 1;
	setsockopt(ra.socket, IPPROTO_TCP, TCP_NODELAY, (void *)&nodelayopt, sizeof(nodelayopt));

	ra.connection = connect(ra.socket, res->ai_addr, res->ai_addrlen);
	if (ra.connection < 0)
	{
		current_error = errno;
		if (current_error != EINPROGRESS)
		{
			ra.connecting = false;
			ra.connected = false;
			gi.dprintf("=RemoteAdmin= problems connecting (%d)\n", current_error);
			return;
		}
	}
	ra.connecting = false;
	ra.connected = true;
}


/*
 * close remote connection
 *
 */
void Ra_Disconnect()
{
	if (ra.connected) 
	{
		Ra_Send("quit");
	}
	ra.connected = false;
	ra.connecting = false;
	ra.authed = false;
	ra.authing = false;
	ra.ready = false;
}


char *va(const char *format, ...) 
{
	static char strings[8][MAX_STRING_CHARS];
	static uint16_t index;

	char *string = strings[index++ % 8];

	va_list args;

	va_start(args, format);
	vsnprintf(string, MAX_STRING_CHARS, format, args);
	va_end(args);

	return string;
}


ssize_t sslReadLine(int fd, void *buffer, size_t n)
{
    ssize_t numRead;                    /* # of bytes fetched by last read() */
    size_t totRead;                     /* Total bytes read so far */
    char *buf;
    char ch;

    if (n <= 0 || buffer == NULL) 
	{
        errno = EINVAL;
        return -1;
    }

    buf = buffer;                       /* No pointer arithmetic on "void *" */

    totRead = 0;
    for (;;) {
        numRead = read(fd, &ch, 1);

        if (numRead == -1) 
		{
            if (errno == EINTR)         /* Interrupted --> restart read() */
                continue;
            else
                return -1;              /* Some other error */

        } 
		else if (numRead == 0) 
		{      /* EOF */
            if (totRead == 0)           /* No bytes read; return 0 */
                return 0;
            else                        /* Some bytes read; add '\0' */
                break;

        } 
		else 
		{                        /* 'numRead' must be 1 if we get here */
            if (totRead < n - 1) 
			{      /* Discard > (n - 1) bytes */
                totRead++;
                *buf++ = ch;
            }

            if (ch == '\n')
                break;
        }
    }

    *buf = '\0';
    return totRead;
}

// strip out all but printable characters and stop at a newline
char *printableLine(char *input)
{
	char out[256];
	int i,j;
	for (i=0,j=0; i<sizeof(input); i++ )
	{
		// printable chars only
		if (input[i] >= 32 && input[i] <= 127 )
		{
			out[j] = input[i];
			j++;
		}

		// if output buffer is full or we hit a new line in input
		if (j == 255 || input[i] == '\n')
		{
			break;
		}
	}
	
	out[sizeof(out)] = '\0'; 
	return out;
}

void Ra_Status_f(void)
{
	if (ra.enabled)
	{
		gi.dprintf("** RA is enabled\n");
	}
	else
	{
		gi.dprintf("** RA is currently disabled\n");
		return;
	}

	if (ra.connected)
	{
		gi.dprintf("** RA is connected to %s:$s\n", g_ra_addr->string, g_ra_port->string);
	}
}

void Ra_Connect_f(void)
{
	if (ra.connected)
	{
		Ra_Disconnect_f();
	}

	Ra_Init();
}

void Ra_Disconnect_f(void)
{
	if (ra.connected)
	{
		Ra_Disconnect();
		return;
	}

	gi.dprintf("=RA= not connected...\n");
}

/*
 * Called from SpawnEntities whenever a map is loaded
 */
void Ra_NewMap(char *map)
{
	if (ra.connected)
	{
		Ra_Send(va("%d %s", PROTO_NEWMAP, map));
	}
}

/*
 * Called from game's ClientConnect() for every command 
 * a client issues
 */
void Ra_ClientCommand(edict_t *ent)
{
	if (!ra.connected)
		return;
		
	//gi.dprintf("ClientCMD: \"%s %s\"\n", gi.argv(0), gi.args());
	if (strcmp(gi.argv(0), "say") == 0 ||
		strcmp(gi.argv(0), "say_team") == 0 ||
		strcmp(gi.argv(0), "say_person") == 0)
	{
		int clientid = Ra_GetClientNumber(ent);
		Ra_Send(
			va(
				"%d %s %s",
				PROTO_CHAT,
				proxyinfo[clientid].ipaddress,
				gi.args()
			)
		);
	}
}

/*
 * Called from ClientUserinfoChanged() 
 */
void Ra_UserInfoChange(edict_t *ent, char *userinfo)
{
	if (ra.connected)
	{
		uint32_t client = Ra_GetClientNumber(ent);
		char *ip = proxyinfo[client].ipaddress;
		Ra_Send(va("%d %s %s", PROTO_USERINFO, ip, userinfo)); 
	}
}

