### Q2Admin Makefile ###

-include .config

CONFIG_HTTP = 1
#V = 1
ifndef CPU
    CPU := $(shell uname -m | sed -e s/i.86/i386/ -e s/amd64/x86_64/ -e s/sun4u/sparc64/ -e s/arm.*/arm/ -e s/sa110/arm/ -e s/alpha/axp/)
endif

ifndef REV
    REV := $(shell git rev-list HEAD | wc -l)
endif

ifndef VER
    VER := 1.18-r$(REV)~$(shell git rev-parse --short HEAD)
endif

CC ?= gcc
WINDRES ?= windres
STRIP ?= strip
RM ?= rm -f

CFLAGS ?= -ffast-math -w -ggdb3 -fPIC $(INCLUDES)
LDFLAGS ?= -shared -lssl -lm -lcurl -ldl -lcrypto
LIBS ?=

ifdef CONFIG_WINDOWS
    LDFLAGS += -mconsole
    LDFLAGS += -Wl,--nxcompat,--dynamicbase
else
    CFLAGS += -DLINUX
    LDFLAGS += -Wl,--no-undefined -DLINUX
endif

CFLAGS += -DQ2ADMINVERSION='"$(VER)"' -DGAME_INCLUDE
RCFLAGS += -DQ2ADMINVERSION='\"$(VER)\"' -DGAME_INCLUDE

ifdef CONFIG_HTTP
    CURL_CFLAGS ?= $(shell pkg-config libcurl --cflags)
    CURL_LIBS ?= $(shell pkg-config libcurl --libs)
    CFLAGS += -DHAVE_CURL $(CURL_CFLAGS)
    LIBS += $(CURL_LIBS)
    LDFLAGS += -lcurl
endif

OBJS := fopen.o g_main.o md4.o ra_main.o regex.o q2a_acexcp.o q2a_ban.o \
q2a_checkvar.o q2a_clib.o q2a_cmd.o q2a_disable.o q2a_flood.o q2a_hashl.o q2a_init.o \
q2a_log.o q2a_lrcon.o q2a_msgqueue.o q2a_spawn.o q2a_util.o q2a_vote.o q2a_zbotcheck.o q2a_zbot.o

ifdef CONFIG_WINDOWS
    CPU := x86
    #OBJS += sys_win32.o
    #OBJS += opentdm.o
    ifdef CONFIG_HTTP
        LIBS += -lws2_32
    endif
    TARGET := game$(CPU)-q2admin-$(VER)-pf.dll
else
    #OBJS += sys_linux.o
    LIBS += -lm
    TARGET := game$(CPU)-q2admin-$(VER)-pf.so
endif

ifdef CONFIG_LINUX32
	CFLAGS += -m32
	LDFLAGS += -m32
	CPU := x86
	TARGET := game$(CPU)-q2admin-$(VER)-pf.so
endif

all: $(TARGET)

default: all

.PHONY: all default clean strip

# Define V=1 to show command line.
ifdef V
    Q :=
    E := @true
else
    Q := @
    E := @echo
endif

-include $(OBJS:.o=.d)

%.o: %.c
	$(E) [CC] $@
	$(Q)$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.rc
	$(E) [RC] $@
	$(Q)$(WINDRES) $(RCFLAGS) -o $@ $<

$(TARGET): $(OBJS)
	$(E) [LD] $@
	$(Q)$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	$(E) [CLEAN]
	$(Q)$(RM) *.o *.d $(TARGET)

strip: $(TARGET)
	$(E) [STRIP]
	$(Q)$(STRIP) $(TARGET)

